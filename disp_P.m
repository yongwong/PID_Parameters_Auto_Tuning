function [TTr,t,y] = disp_P(Zp,num0,den0)
%显示纯比例整定结果,返回衰减时间
   t=0:0.01:30;
   sys1=tf(1/Zp);                           %PID部分传递函数 
   [num,den]=GouZaotf(sys1,num0,den0);      %待整定传递函数系数
   sys=tf(num,den);
   y=step(sys,t);
   axis([0 30 0 1.5]);
   %调用DongTaiZhiBiao函数，返回上升时间Tr、调节时间Ts、衰减比sjb
   [Tr,Ts,~,TTr,JingCha]=DongTaiZhiBiao(num,den);
end

